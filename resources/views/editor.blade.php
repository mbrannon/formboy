@extends('voyager::master')

@section('content')

<div class="container-fluid">
	<h1 class="page-title">
		<i class="voyager-window-list"></i> Edit {{ $survey->name }} Layout
	</h1>
</div>

<div class="page-content browse container-fluid">
	<div class="row">
		<div class="col-md-12">
			<form id='survey-builder'>
				<input type='hidden' name='survey_id' value='{{ $survey->id }}'>
				<ul id='survey-builder-sort'>
					@foreach ($fields as $field)
						@if ($field->parent == "")
							@switch($field->type)
								@case('copy')
								@case('text')
								@case('textarea')
									@include('editor.field.generic')
									@break
								@case('table')
								@case('repeater')
									@include('editor.field.table')
									@break
							@endswitch
						@endif
					@endforeach
				</ul>
			</form>
			<a data-type='copy' class="add-survey-field-btn btn btn-success btn-add-new">
				<i class="voyager-plus"></i> <span>Add Copy</span>
			</a>
			<a data-type='text' class="add-survey-field-btn btn btn-success btn-add-new">
				<i class="voyager-plus"></i> <span>Add Text</span>
			</a>
			<a data-type='textarea' class="add-survey-field-btn btn btn-success btn-add-new">
				<i class="voyager-plus"></i> <span>Add Textarea</span>
			</a>
			<a data-type='table' class="add-survey-field-btn btn btn-success btn-add-new">
				<i class="voyager-plus"></i> <span>Add Table</span>
			</a>
			<a data-type='repeater' class="add-survey-field-btn btn btn-success btn-add-new">
				<i class="voyager-plus"></i> <span>Add Repeater</span>
			</a>
			<button id='survey-builder-save' type="button" class="btn btn-primary pull-right">Save Survey</button>
		</div>
	</div>
</div>

<div id='survey-editor-storage' style='display:none;'>
	@php
		// reset field variable for placeholders
		$field = (object) ["type" => "", "hash" => "", "parent" => "", "title" => "", "copy" => ""];
	@endphp
	@include('editor.field.blank')
	@include('editor.field.generic')
	@include('editor.field.table')
</div>

@stop

