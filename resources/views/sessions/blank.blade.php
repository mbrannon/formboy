<li class='session-template' 
	@if(isset($session))
	id='session-{{ $session->hash }}'
	@endif
>
	<div class="panel panel-bordered">
		<div class="panel-body">
			<div class='row session-row'>
				<div class="col-md-4 user">
					@if(isset($session))
						@foreach ($users as $user)
							@if ($user->id == $session->user_id)
								{{ $user->name }} ({{ $user->email }})
							@endif
						@endforeach
					@endif
				</div>
				<div class="col-md-3 survey">
					@if(isset($session))
						@foreach ($surveys as $survey)
							@if ($survey->id == $session->survey_id)
								{{ $survey->name }}
							@endif
						@endforeach
					@endif
				</div>
				<div class="col-md-2 time text-center">
					@if(isset($session))
						{{ $session->created_at->diffForHumans() }}
					@endif
				</div>
				<div class="col-md-2 link text-center">
					<a target='_blank' 
						@if(isset($session))
							href='/survey/{{ $session->hash }}' 
						@endif
					>&nwarr; View</a>
				</div>
				<div class="col-md-1">
					<span class='icon voyager-trash remove-session-btn pull-right'
						@if(isset($session))
							data-id='{{ $session->id }}' 
						@endif
					></span>
				</div>
			</div>
		</div>
	</div>
</li>