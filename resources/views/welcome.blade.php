<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>FormBoy</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links.shake {
                animation: shake 0.82s cubic-bezier(.36,.07,.19,.97) both;
                transform: translate3d(0, 0, 0);
                backface-visibility: hidden;
                perspective: 1000px;
            }

            .links.shake input[type=text] {
                border-color:#ff2b1a;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            input[type=text] {
                width: 300px;
                height: 35px;
                line-height: 35px;
                padding: 5px 10px;
                border-radius: 5px;
                font-size: 20px;
                outline: 0;
                -webkit-appearance: none;
                -moz-appearance: none;
                border: 2px solid #636b6f;
                transition: all .2s ease-in-out;
                display: inline-block;
                vertical-align: middle;
            }
            input[type=text]:focus {
                box-shadow: 0 10px 20px rgba(0,0,0,.2);
            }

            a.button {
                color: #fff;
                font-weight: 800;
                font-size: 14px;
                background: #ff2b1a;
                padding: 12px;
                text-transform: uppercase;
                border: none !important;
                border-radius: 5px;
                height:49px;
                line-height:27px;
                transition: all .2s ease-in-out;
                display: inline-block;
                vertical-align: middle;
                box-sizing: border-box;
            }
            a.button:hover {
                color: #fff !important;
                background: #243641;
                box-shadow: 0 10px 20px rgba(0,0,0,.2);
                cursor:pointer;
            }
            @keyframes shake {
                10%, 90% {
                    transform: translate3d(-1px, 0, 0);
                }

                20%, 80% {
                    transform: translate3d(2px, 0, 0);
                }

                30%, 50%, 70% {
                    transform: translate3d(-4px, 0, 0);
                }

                40%, 60% {
                    transform: translate3d(4px, 0, 0);
                }
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="top-right links">
                @auth
                    <a href="/admin">Dashboard</a>
                @else
                    <a href="/admin/login">Login</a>
                @endauth
            </div>
            <div class="content">
                <div class="title m-b-md">
                    FormBoy
                </div>
                <div class="links" id="links">
                    <input type='text' id='surveyhash' placeholder=''>
                    <a class='button' id='go-btn'>GO</a>
                </div>
                <p>Enter your form hash to get started.</p>
            </div>
        </div>
        <script type='text/javascript'>
            document.addEventListener('click', function (event) {
                if (!event.target.matches('#go-btn')) return;
                event.preventDefault();
                viewForm();
            }, false);
            var surveyhash = document.getElementById('surveyhash');
            surveyhash.onkeydown = function(e) {
                if (e.key == "Enter") {
                    viewForm();
                }
            };
            function viewForm() {
                var hash = document.getElementById('surveyhash').value;
                if (hash.length > 30) {
                    location.href = "/survey/"+hash;
                } else {
                    document.getElementById('links').classList.add('shake');
                    setTimeout(function(){
                        document.getElementById('links').classList.remove('shake');
                    }, 1000);
                }
            }
        </script>
    </body>
</html>
