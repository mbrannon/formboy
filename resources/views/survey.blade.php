<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>FormBoy | {{ $surveyTitle[0] }}</title>
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <script type="text/javascript" src="{{ asset('js/jq.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/survey.js') }}"></script>
    </head>
    <body class='survey-frontend'>
    	<div class="top-right links" id="websocket-output">
            Connecting
        </div>
        <div class="position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    {{ $surveyTitle[0] }}
                </div>
                <form action='/survey/save' id='survey-form' method='POST'>
                	@csrf <!-- {{ csrf_field() }} -->
                	<input type='hidden' name='surveyHash' id='survey-form-hash' value='{{ $surveySession->hash }}'>
                	@foreach ($fields as $field)
						@if ($field->parent == "")
                            @include("survey.field.{$field->type}")
						@endif
					@endforeach
					<a class='button save-survey' id='save-survey'>SAVE SURVEY</a>
            	</form>
            </div>
        </div>
        <script type='text/javascript'>
        	/* <![CDATA[ */
				var socketUrl = "{{ $socketUrl }}";
			/* ]]> */
        </script>
    </body>
</html>
