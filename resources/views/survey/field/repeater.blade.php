<div class="panel panel-bordered panel-repeater">
	<div class="panel-body">
		<h3>{{ $field->title }}</h3>
		<p>{{ $field->copy }}</p>
		<table id='table-{{ $field->hash }}' class='repeater'>
			<tr>
				@php
					$row_count = 0;
				@endphp
				@foreach ($fields as $subfield)
					@if ($subfield->parent == $field->hash && $field->hash != "")
						@php
							if ($row_count == 0) {
								$row_count = (isset($fieldData[$subfield->hash]) ? sizeof($fieldData[$subfield->hash]) : 0);
							}
						@endphp
						<th>
							<strong>{{ $subfield->title }}</strong>
							<span>{{ $subfield->copy }}</span>
						</th>
					@endif
				@endforeach
				<th>{{-- held for removal icons --}}</th>
			</tr>
			@for ($i = 0; $i < $row_count; $i++)
				<tr>
					@foreach ($fields as $subfield)
						@if ($subfield->parent == $field->hash && $field->hash != "")
							<td>
								<input type='text' name='{{ $subfield->hash }}[]' class='socket-editable field-{{ $subfield->hash }}' value='{{ $fieldData[$subfield->hash][$i] ?? '' }}' data-repeat='true' data-table='table-{{ $field->hash }}' data-field='field-{{ $subfield->hash }}'>
							</td>
						@endif
					@endforeach
					<td><img src='{{ asset('img/remove.png') }}' data-table='table-{{ $field->hash }}' class='remove-icon remove-{{ $subfield->hash }}'></td>
				</tr>
			@endfor
			@if ($row_count == 0)
				<tr>
					@foreach ($fields as $subfield)
						@if ($subfield->parent == $field->hash && $field->hash != "")
							<td>
								<input type='text' name='{{ $subfield->hash }}[]' class='socket-editable field-{{ $subfield->hash }}' value='' data-repeat='true' data-table='table-{{ $field->hash }}' data-field='field-{{ $subfield->hash }}'>
							</td>
						@endif
					@endforeach
					<td><img src='{{ asset('img/remove.png') }}' data-table='table-{{ $field->hash }}' class='remove-icon remove-{{ $subfield->hash }}'></td>
				</tr>
			@endif
		</table>
		<a class='button add-repeater-row' id='add-{{ $field->hash }}' data-id='add-{{ $field->hash }}' data-table='table-{{ $field->hash }}'>ADD A ROW</a>
	</div>
</div>