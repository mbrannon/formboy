<div class="panel panel-bordered">
	<div class="panel-body">
		<h4>{{ $field->title }}:</h4>
		<p>{{ $field->copy }}</p>
		<input type='text' name='{{ $field->hash }}' class='socket-editable field-{{ $field->hash }}' id='field-{{ $field->hash }}' value='{{ $fieldData[$field->hash][0] ?? '' }}' data-repeat='false'>
	</div>
</div>