<div class="panel panel-bordered">
	<div class="panel-body">
		<h3>{{ $field->title }}</h3>
		<p>{{ $field->copy }}</p>
		<table>
			<tr>
				@foreach ($fields as $subfield)
					@if ($subfield->parent == $field->hash && $field->hash != "")
						@include('survey.field.subfield')
						@if ($loop->even) 
							</tr><tr>
						@endif
					@endif
				@endforeach
			</tr>
		</table>
	</div>
</div>