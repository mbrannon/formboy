<td>
	<strong>{{ $subfield->title }}:</strong>
	<input type='text' name='{{ $subfield->hash }}' class='socket-editable field-{{ $subfield->hash }}' id='field-{{ $subfield->hash }}' value='{{ $fieldData[$subfield->hash][0] ?? '' }}' data-repeat='false'>
</td>