<div class="panel panel-bordered">
	<div class="panel-body">
		<h4>{{ $field->title }}:</h4>
		<p>{{ $field->copy }}</p>
		<textarea name='{{ $field->hash }}' class='socket-editable field-{{ $field->hash }}' id='field-{{ $field->hash }}' data-repeat='false'>{{ $fieldData[$field->hash][0] ?? '' }}</textarea>
	</div>
</div>