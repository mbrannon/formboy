<div class="panel panel-bordered panel-copy">
	<div class="panel-body">
		<h2>{{ $field->title }}</h2>
		<p>{{ $field->copy }}</p>
	</div>
</div>