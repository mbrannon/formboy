@extends('voyager::master')

@section('content')

<div class="container-fluid">
	<h1 class="page-title">
		<i class="voyager-paper-plane"></i> Sessions
	</h1>
</div>

<div class="page-content browse container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-bordered">
				<div class="panel-body">
					<h3>New Survey Session</h3>
					<form id='add-new-session-form'>
						<div class='row'>
							<div class="col-md-5">
								<select name='user' id='session-select-user'>
									@foreach ($users as $user)
										<option value='{{ $user->id }}'>{{ $user->name }} ({{ $user->email }})</option>
									@endforeach
								</select>
							</div>
							<div class="col-md-5">
								<select name='survey' id='session-select-survey'>
									@foreach ($surveys as $survey)
										<option value='{{ $survey->id }}'>{{ $survey->name }}</option>
									@endforeach
								</select>
							</div>
							<div class="col-md-2">
								<button id='survey-session-add' type="button" class="btn btn-primary center-block">Add Session</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<ul id='survey-sessions'>
				@foreach ($sessions as $session)
					@include('sessions.blank')
				@endforeach
			</ul>
		</div>
	</div>
</div>

<div id='survey-session-storage' style='display:none;'>
	@include('sessions.blank')
</div>

@stop

