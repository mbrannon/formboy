<li class='field-type-generic'>
	<div class="panel panel-bordered">
		<div class="panel-body">
			<h3><span class="panel-type-name">{{ $subfield->type }}</span> Field</h3>
			<input type="hidden" name="formhash" class='form-hash-hidden' value='{{ $subfield->hash }}'>
			<input type="hidden" name="formtype" class='form-type-hidden' value='{{ $subfield->type }}'>
			<input type="hidden" name="parenthash" class='parent-hash-hidden' value='{{ $subfield->parent }}'>
			<h6>Header</h6>
			<input type="text" class="form-control input-md" name="formtitle" value="{{ $subfield->title }}">
			<h6>Subline</h6>
			<input type="text" class="form-control input-md" name="formcopy" value="{{ $subfield->copy }}">
			<div class='field-panel-controls'>
				<span class='icon voyager-trash remove-field-btn'></span>
				<span class='icon voyager-handle'></span> 
			</div>
		</div>
	</div>
</li>