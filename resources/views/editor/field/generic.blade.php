<li class='field-type-generic'	

	@if ($field->hash != "")
		id='field-{{ $field->hash }}'
	@endif
	
>
	<div class="panel panel-bordered">
		<div class="panel-body">
			<h3><span class="panel-type-name">{{ $field->type }}</span> Field</h3>
			<input type="hidden" name="formhash" class='form-hash-hidden' value='{{ $field->hash }}'>
			<input type="hidden" name="formtype" class='form-type-hidden' value='{{ $field->type }}'>
			<input type="hidden" name="parenthash" class='parent-hash-hidden' value='{{ $field->parent }}'>
			<h6>Header</h6>
			<input type="text" class="form-control input-md" name="formtitle" value="{{ $field->title }}">
			<h6>Subline</h6>
			<input type="text" class="form-control input-md" name="formcopy" value="{{ $field->copy }}">
			<div class='field-panel-controls'>
				<span class='icon voyager-trash remove-field-btn' 

					@if ($field->hash != "")
						data-id='field-{{ $field->hash }}'
					@endif

				></span>
				<span class='icon voyager-handle'></span> 
			</div>
		</div>
	</div>
</li>