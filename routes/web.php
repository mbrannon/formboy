<?php

use Illuminate\Support\Facades\Route;
use BeyondCode\LaravelWebSockets\Facades\WebSocketsRouter;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/survey/{hash}','SurveyFillController@view');
Route::post('/survey/save','SurveyFillController@save');

WebSocketsRouter::webSocket('/socket', \App\CustomWebSocketHandler::class);

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    // surveys
    Route::get('surveys/editor/{id}','SurveyEditController@edit')->name('edit.survey');
    Route::post('surveys/editor/save','SurveyEditController@save')->name('save.survey');
    // sessions
    Route::get('sessions','SurveySessionController@view')->name('view.sessions');
    Route::post('sessions/add','SurveySessionController@add')->name('add.sessions');
    Route::post('sessions/remove','SurveySessionController@remove')->name('remove.sessions');
});

