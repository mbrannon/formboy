jQuery(document).ready(function() {
	var websocket;
	var clickIn = false;
	var changeCountdown;
	jQuery(".add-repeater-row").click(function(){
		var table = jQuery(this).data('table');
		var field = jQuery(this).data('id');
		jQuery("#"+table+" tr:last-child").clone().appendTo("#"+table);
		jQuery("#"+table+" tr:last-child input[type=text]").val('');
		if (!clickIn) { sendFormAction(field, 0); }
		clickIn = false;
	});
	jQuery("#save-survey").click(function(){
		jQuery("#survey-form").submit();
	});
	jQuery("#survey-form").on('click', '.remove-icon', function(){
		var table = jQuery(this).data('table');
		var row = jQuery(this).parent('td').parent('tr').index();
		if (jQuery("#"+table+" tr").length <= 2) {
			jQuery("#"+table+" input[type=text]").val('');
		} else {
			jQuery(this).parent('td').parent('tr').remove();
		}
		if (!clickIn) { sendFormAction(table, row); }
		clickIn = false;
	});
	jQuery("#survey-form").on('keyup', '.socket-editable', function(){
		var self = this;
		clearTimeout(changeCountdown);
		changeCountdown = setTimeout(function(){
			var room = jQuery("#survey-form-hash").val();
			var repeat = jQuery(self).data('repeat');
			var value = jQuery(self).val();
			var field = '';
			var row = 0;
			if (repeat) {
				// pull field tag from table data
				field = jQuery(self).data('field');
				row = jQuery(self).parent('td').parent('tr').index() - 1;
			} else {
				field = jQuery(self).attr('id');
			}
			var content = {
				room:room,
				field:field,
				value:value,
				row:row
			};
			doSend(JSON.stringify({event:'modify-form', content:content}));
		}, 100);
	});
	function sendFormAction(field, row) {
		var room = jQuery("#survey-form-hash").val();
		var content = {
			room:room,
			field:field,
			row:row
		};
		doSend(JSON.stringify({event:'button-action', content:content}));
	}
	function startSocket() {
		websocket = new WebSocket(socketUrl);
		websocket.onopen = function(evt) { onOpen(evt) };
		websocket.onclose = function(evt) { onClose(evt) };
		websocket.onmessage = function(evt) { onMessage(evt) };
		websocket.onerror = function(evt) { onError(evt) };
	}
	function doSend(message) {
		if (typeof websocket != 'undefined') {
			if (websocket.readyState == 1) {
				websocket.send(message);
			}
		}
	}
	function isJsonString(str) {
		try {
			JSON.parse(str);
		} catch (e) {
			return false;
		}
		return true;
	}
	function onOpen(evt) {
		doSend(JSON.stringify({event:'subscribe', content:jQuery("#survey-form-hash").val()}));
	}
	function onClose(evt) { 
		socketClosed();
	}
	function onError(evt) {
		socketClosed();
	}
	function onMessage(evt) {
		if (!isJsonString(evt.data)) { return; }
		const obj = JSON.parse(evt.data);
		if (obj.action == "subscribed") {
			jQuery("#websocket-output").html("Connected");
		}
		if (obj.action == "modify-form") {
			jQuery("."+obj.data.field+":eq("+obj.data.row+")").val(obj.data.value);
		}
		if (obj.action == "button-action") {
			var selector = "";
			if (obj.data.field.includes('table-')) {
				selector = "#"+obj.data.field+" tr:eq("+obj.data.row+") .remove-icon";
			} 
			if (obj.data.field.includes('add-')) {
				selector = "#"+obj.data.field;
			}
			clickIn = true;
			jQuery(selector).click();
		}
	}
	function socketClosed() {
		jQuery("#websocket-output").html("Disconnected");
		setTimeout(function() {
			jQuery("#websocket-output").html("Reconnecting");
			startSocket();
		},10000);
	}
	startSocket();
});
