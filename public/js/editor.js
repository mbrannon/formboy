jQuery.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	jQuery.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || " ");
		} else {
			o[this.name] = this.value || " ";
		}
	});
	return o;
};
jQuery(document).ready(function() {
	function showBlank() {
		if (jQuery("#survey-builder-sort  li").length < 1) {
			jQuery("#survey-editor-storage li.field-type-none").clone().appendTo("#survey-builder-sort");
		}
	}
	function setupSort() {
		jQuery("#survey-builder-sort").sortable();
		jQuery(".subsort").sortable({
			connectWith: ".subsort"
		});
	}
	function makeHash(length) {
		var result = [];
		var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		var charactersLength = characters.length;
		for (var i = 0; i < length; i++) {
			result.push(characters.charAt(Math.floor(Math.random() * charactersLength)));
		}
		return result.join("");
	}
	jQuery(".add-survey-field-btn").click(function(){
		if (jQuery("#survey-builder-sort  li.field-type-none").length > 0) {
			jQuery("#survey-builder-sort  li.field-type-none").remove();
		}
		var type = jQuery(this).data("type");
		var hash = makeHash(32);
		var field = "field-"+hash;
		var fieldtype = "field-type-generic";
		switch (type) {
			case "repeater":
			case "table":
				fieldtype = "field-type-table";
				break;
		}
		jQuery("#survey-editor-storage li."+fieldtype).clone().data("type", type).data("hash", hash).attr("id", field).appendTo("#survey-builder-sort");
		jQuery("#"+field+" ul.subsort").empty();
		jQuery("#"+field+" input[type=text]").val('');
		jQuery("#"+field+" .form-hash-hidden").val(hash);
		jQuery("#"+field+" .form-type-hidden").val(type);
		jQuery("#"+field+" .panel-type-name").html(type);
		jQuery("#"+field+" .remove-field-btn, #"+field+" .add-field-btn").data("id", field);
		setupSort();
	});
	jQuery("#survey-builder-sort").on("click",".remove-field-btn",function(){
		var field = jQuery(this).data("id");
		jQuery("#"+field).remove();
		if (jQuery("#survey-builder-sort li").length < 1) {
			showBlank();
		}
		setupSort();
	});
	jQuery("#survey-builder-sort").on("click","a.add-field-btn",function(){
		var parent_field = jQuery(this).data("id");
		var parent_hash = parent_field.replace("field-", "");
		var hash = makeHash(32);
		var field = "field-"+hash;
		jQuery("#survey-editor-storage > li.field-type-generic").clone().data("type", "text").data("hash", hash).attr("id", field).appendTo("#"+parent_field+" .subsort");
		jQuery("#"+field+" input[type=text]").val('');
		jQuery("#"+field+" .form-hash-hidden").val(hash);
		jQuery("#"+field+" .form-type-hidden").val("text");
		jQuery("#"+field+" .parent-hash-hidden").val(parent_hash);
		jQuery("#"+field+" .panel-type-name").html("Text");
		jQuery("#"+field+" .remove-field-btn").data("id", field);
		setupSort();
	});
	jQuery("#survey-builder-save").click(function(){
		if (!jQuery(this).hasClass('clicked')) {
			var self = this;
			jQuery(self).addClass('clicked');
			var formData = jQuery("#survey-builder").serializeObject();
			var json = JSON.stringify(formData);
			jQuery.post('/admin/surveys/editor/save',{data: json},function(data){
				jQuery(self).removeClass('clicked');
				if (data.status == "ok") {
					location.href = "/admin/surveys";
				} else {
					alert("There was a problem saving your Survey. Please try again later.");
				}
			});
		}
	});
	showBlank();
	setupSort();
});
