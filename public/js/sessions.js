jQuery(document).ready(function() {
	jQuery("#survey-session-add").click(function(){
		if (!jQuery(this).hasClass('clicked')) {
			var self = this;
			jQuery(self).addClass('clicked');
			var user_id = jQuery("#session-select-user").find(":selected").val();
			var survey_id = jQuery("#session-select-survey").find(":selected").val();
			jQuery.post('/admin/sessions/add',{user_id: user_id, survey_id: survey_id},function(data){
				jQuery(self).removeClass('clicked');
				if (data.hash) {
					var ident = "session-"+data.hash;
					jQuery("#survey-session-storage li.session-template").clone().attr("id", ident).prependTo("#survey-sessions");
					jQuery("#survey-sessions #"+ident+" .user").html(data.user);
					jQuery("#survey-sessions #"+ident+" .survey").html(data.survey);
					jQuery("#survey-sessions #"+ident+" .time").html(data.time);
					jQuery("#survey-sessions #"+ident+" .link a").attr('href','/survey/'+data.hash);
					jQuery("#survey-sessions #"+ident+" .remove-session-btn").data('id', data.id);
				} else {
					alert("There was a problem saving your Session. Please try again later.");
				}
			});
		}
	});
	jQuery("#survey-sessions").on("click",".remove-session-btn",function(){
		if (!jQuery(this).hasClass('clicked')) {
			var self = this;
			jQuery(self).addClass('clicked');
			var session_id = jQuery(this).data("id");
			jQuery.post('/admin/sessions/remove',{session_id: session_id},function(data){
				jQuery(self).removeClass('clicked');
				if (data.session) {
					jQuery("#session-"+data.session).remove();
				} else {
					alert("There was a problem removing your Session. Please try again later.");
				}
			});
		}
	});
});
