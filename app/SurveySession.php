<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveySession extends Model
{

    protected $fillable = ['user_id', 'survey_id', 'hash', 'is_active'];

    /**
     * Get the user associated with the session.
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Get the survey associated with the session.
     */
    public function survey()
    {
        return $this->hasOne(Survey::class, 'id', 'survey_id');
    }
}
