<?php
namespace App;

use Ratchet\ConnectionInterface;
use Ratchet\RFC6455\Messaging\MessageInterface;
use Ratchet\WebSocket\MessageComponentInterface;
use Log;

class CustomWebSocketHandler implements MessageComponentInterface
{
    protected $clients;
    private $subscriptions;
    private $users;

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
        $this->subscriptions = [];
        $this->users = [];
    }

    public function onOpen(ConnectionInterface $connection)
    {
        $this->clients->attach($connection);
        $this->users[$connection->resourceId] = $connection;
        // make pusher shut up. we're not using it.
        $socketId = sprintf('%d.%d', random_int(1, 1000000000), random_int(1, 1000000000));
        $connection->socketId = $socketId;
        $connection->app = new \stdClass(); 
        $connection->app->id = 'formboy';
    }

    public function onClear(ConnectionInterface $connection) {
        $this->clients->detach($connection);
        unset($this->users[$connection->resourceId]);
        // clear user from subscription(s)
        foreach ($this->subscriptions as $room => $subscriptions) {
            // check all users in this room
            foreach ($subscriptions as $key => $subscription) {
                // if we have a match, prune
                if ($subscription == $connection->resourceId) {
                    unset($this->subscriptions[$room][$key]);
                }
            }
        }
    }
    
    public function onClose(ConnectionInterface $connection)
    {
        $this->onClear($connection);
    }

    public function onError(ConnectionInterface $connection, \Exception $e)
    {
        $this->onClear($connection);
    }

    public function onMessage(ConnectionInterface $connection, MessageInterface $msg)
    {
        // decode our json and quick check fields
        try {
            $data = json_decode($msg);
            $event = $data->event;
            $content = $data->content;
        } catch (\Exception $e) {
            Log::error($e);
            return;
        }

        if ($event == 'subscribe') {
            // $content is hash of survey form
            $this->subscriptions[$content][] = $connection->resourceId;
            $this->sendToUser($connection->resourceId, json_encode([
                'action' => 'subscribed',
                'data' => 'ok',
            ]));
        }

        if ($event == 'button-action') {
            // this is a change to the form from a user
            try {
                $room = $content->room;
                $field = $content->field;
                $row = $content->row;
            } catch (\Exception $e) {
                Log::error($e);
                return;
            }
            // preencode data
            $message = json_encode([
                'action' => 'button-action',
                'data' => [
                    'field' => $field,
                    'row' => $row,
                ]
            ]);
            // loop through all users on this survey
            foreach ($this->subscriptions[$room] as $key => $subscription) {
                // ensure we dont send modification back to sending user
                if ($subscription != $connection->resourceId) {
                    $this->sendToUser($subscription, $message);
                }
            }
        }

        if ($event == 'modify-form') {
            // this is a change to the form from a user
            try {
                $room = $content->room;
                $field = $content->field;
                $value = $content->value;
                $row = $content->row;
            } catch (\Exception $e) {
                Log::error($e);
                return;
            }
            // preencode data
            $message = json_encode([
                'action' => 'modify-form',
                'data' => [
                    'field' => $field,
                    'value' => $value,
                    'row' => $row,
                ]
            ]);
            // loop through all users on this survey
            foreach ($this->subscriptions[$room] as $key => $subscription) {
                // ensure we dont send modification back to sending user
                if ($subscription != $connection->resourceId) {
                    $this->sendToUser($subscription, $message);
                }
            }
        }
    }

    public function sendToUser($connectionId, $message) {
        // send message to a user
        if (isset($this->users[$connectionId])) {
            $this->users[$connectionId]->send($message);
        }
    }
}