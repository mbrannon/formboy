<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyFieldEntry extends Model
{
    protected $table = 'survey_field_entries';
}
