<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;
use Log;

class ConfigureFieldsAction extends AbstractAction
{
    public function getTitle()
    {
        return 'Edit Layout';
    }

    public function getIcon()
    {
        return 'voyager-settings';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary',
        ];
    }

    public function getDefaultRoute()
    {
        return route('edit.survey',['id' => $this->data->id]);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'surveys';
    }
}