<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyField extends Model
{
	protected $fillable = ['survey_id', 'title', 'copy', 'type', 'hash', 'parent', 'sort', 'is_visible'];
}
