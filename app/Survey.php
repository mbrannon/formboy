<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    public function sessions()
    {
        return $this->hasMany(SurveySession::class, 'survey_id', 'id');
    }

    public function fields()
    {
    	return $this->hasMany(SurveyField::class, 'survey_id', 'id');
    }
}
