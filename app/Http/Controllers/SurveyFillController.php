<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Survey;
use App\SurveyField;
use App\SurveySession;
use App\SurveyFieldEntry;
use Log;

class SurveyFillController extends Controller
{
	public function view($hash)
	{
		// pull main survey session
		$surveySession = SurveySession::where('hash', $hash)
									->where('is_active', 1)
									->first();

		// check for valid session
		if (!$surveySession) {
			abort(404);
		}

		// pull survey title
		$surveyTitle = Survey::where('id', $surveySession->survey_id)
							->pluck('name');

		// check for valid session
		if (!$surveyTitle) {
			abort(404);
		}

		// pull fields
		$fields = SurveyField::where('survey_id', $surveySession->survey_id)
							->where('is_visible', 1)
							->orderBy('sort', 'ASC')
							->get();

		// check for valid fields
		if (!$fields) {
			abort(404);
		}

		// pull data
		$data = collect(SurveyFieldEntry::where('session_id', $surveySession->id)->get());
	
		// format field entries
		$fieldData = [];

		foreach ($data as $fieldEntry) {
			$fieldData[$fieldEntry->survey_field][$fieldEntry->row] = $fieldEntry->content;
		}

		// pull websocket url
		$socketUrl = config('websockets.websocket_path');

		return view('survey', compact('surveySession', 'surveyTitle', 'fields', 'fieldData', 'socketUrl'));
	}

	public function save(Request $request)
	{
		// check for post data
		if (!$request->has(['surveyHash'])) {
			abort(404);
		}

		// pull survey hash
		$surveyHash = $request->input('surveyHash');

		// pull main survey session
		$surveySession = SurveySession::where('hash', $surveyHash)
									->where('is_active', 1)
									->first();

		// check for valid session
		if (!$surveySession) {
			abort(404);
		}

		// collect form input
		$input = $request->all();

		// prune unneeded form fields
		$prunes = [
			'_token',
			'surveyHash',
		];

		foreach ($prunes as $prune) {
			unset($input[$prune]);
		}

		// start inserts array
		$inserts = [];

		// loop and save
		foreach ($input as $key => $data) {
			if (is_array($data)) {
				// row-based repeater field entries
				for ($x = 0; $x < sizeof($data); $x++) {
					if (!is_null($data[$x])) {
						$inserts[] = [
							'session_id'	=> $surveySession->id,
							'survey_field'  => $key, 
							'content'		=> $data[$x],
							'row'           => $x,
						];
					}
				}
			} else {
				// single text entry
				if (!is_null($data)) {
					$inserts[] = [
						'session_id'	=> $surveySession->id,
						'survey_field'  => $key, 
						'content'		=> $data,
						'row'           => 0,
					];
				}
			}
		}

		// clear old fields
		SurveyFieldEntry::where('session_id', $surveySession->id)->delete();

		// make a collection and chunk inserts
		$inserts = collect($inserts);
		$chunks = $inserts->chunk(500);

		foreach ($chunks as $chunk) {
		   DB::table('survey_field_entries')->insert($chunk->toArray());
		}

		return redirect('/');
	}
}
