<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\SurveyReady;
use App\SurveySession;
use App\Survey;
use App\User;
use Log;

class SurveySessionController extends Controller
{
    public function view()
	{
		// pull sessions
		$users = User::all();
		$surveys = Survey::all();
		$sessions = SurveySession::where('is_active', 1)
								->orderBy('created_at', 'DESC')
								->get();

		// check for permissions
		if (!Auth::user()->can('edit', $surveys[0])) {
			abort(404);
		}

		return view('sessions', compact('users','surveys','sessions'));
	}

	public function add(Request $request)
	{
		// check for post data
		if (!$request->has(['user_id', 'survey_id'])) {
			abort(404);
		}

		// quick check
		try {
			$user_id = (int) $request->input('user_id');
			$survey_id = (int) $request->input('survey_id');
		} catch (\Exception $e) {
			Log::error($e);
			abort(404);
		}

		// pull user record
		if (!$user = User::where('id', $user_id)->first()) {
			abort(404);
		}

		// pull survey record
		if (!$survey = Survey::where('id', $survey_id)->first()) {
			abort(404);
		}		

		// check for permissions
		if (!Auth::user()->can('edit', $survey)) {
			abort(404);
		}

		// create session
		$session = new SurveySession;
		$session->user_id = $user->id;
		$session->survey_id = $survey->id;
		$session->hash = hash('sha256', Str::random(60));
		$session->is_active = 1;
		$session->save();

		// in case email is not properly setup
		try {
			Mail::to($user->email)->send(new SurveyReady("/survey/{$session->hash}"));
		} catch (\Exception $e) {
			Log::error($e);
		}
		
		return response()->json([
			'id' => $session->id,
			'user' => "{$user->name} ({$user->email})",
			'survey' => $survey->name,
			'hash' => $session->hash,
			'time' => $session->created_at->diffForHumans(),
		]);
	}

	public function remove(Request $request)
	{
		// check for post data
		if (!$request->has(['session_id'])) {
			abort(404);
		}

		// quick check
		try {
			$session_id = (int) $request->input('session_id');
		} catch (Exception $e) {
			Log::error($e);
			abort(404);
		}

		// pull user record
		if (!$session = SurveySession::where('id', $session_id)->first()) {
			abort(404);
		}

		$session->is_active = 0;
		$session->save();

		return response()->json([
			'session' => $session->hash,
		]);
	}
}
