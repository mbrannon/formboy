<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Survey;
use App\SurveyField;
use Log;

class SurveyEditController extends Controller
{
	public function edit($id)
	{
		// pull main record
		if (!$survey = Survey::where('id', $id)->first()) {
			abort(404);
		}

		// check for permissions
		if (!Auth::user()->can('edit', $survey)) {
			abort(404);
		}

		// pull fields
		$fields = SurveyField::where('survey_id', $survey->id)
							->where('is_visible', 1)
							->orderBy('sort', 'ASC')
							->get();

		return view('editor', compact('survey', 'fields'));
	}

	public function save(Request $request)
	{
		// check for post data
		if (!$request->has('data')) {
			abort(404);
		}

		// quick json decode & field check
		try {
			$data = json_decode($request->input('data'));
			$survey_id = (int) $data->survey_id;
			$survey_fields = [
				'hash' 		=> $data->formhash,
				'parent' 	=> $data->parenthash,
				'title' 	=> $data->formtitle,
				'copy' 		=> $data->formcopy,
				'type' 		=> $data->formtype,
			];
		} catch (\Exception $e) {
			Log::error($e);
			abort(404);
		}

		// pull survey record
		if (!$survey = Survey::where('id', $survey_id)->first()) {
			abort(404);
		}

		// check for permissions
		if (!Auth::user()->can('edit', $survey)) {
			abort(404);
		}

		// hide old fields
		SurveyField::where('is_visible', 1)
					->where('survey_id', $survey->id)
					->update(['is_visible' => 0]);

		// count modules
		$rows = (is_array($survey_fields['hash']) ? sizeof($survey_fields['hash']) : 1);

		for ($x=0; $x < $rows; $x++) {
			SurveyField::updateOrCreate(
				[
					'survey_id' 	=> $survey_id, 
					'hash' 			=> trim($survey_fields['hash'][$x]),
				],
				[
					'title' 		=> trim($survey_fields['title'][$x]),
					'copy' 			=> trim($survey_fields['copy'][$x]),
					'type' 			=> trim($survey_fields['type'][$x]),
					'parent' 		=> trim($survey_fields['parent'][$x]),
					'sort'			=> $x,
					'is_visible'	=> 1,
				],
			);
		}

		return response()->json([
			'status' => 'ok',
		]);
	}
	
}

